### Tutor 07

> Features

* Blog Pagination: Adjacent, Indicator, Responsive

* Design: Apply Box Design, for Tag, and Archive

* Widget: Friends, Archives Tree, Categories, Tags, Recent Post, Related Post

> Refactoring with Nunjucks

* Layout Inheritance: single-double

* Partials: Page (blog-header), Post (blog-header), Widget (template)

* Nunjucks: Color using Template Inheritance

> Configuration

* Filter: limit, values, keys, shuffle, hashIt, pagerIt

* Collection: postsPrevNext

![Eleventy Bulma MD: Tutor 06][11ty-bulma-md-preview-06]

-- -- --

What do you think ?

[11ty-bulma-md-preview-06]:https://gitlab.com/epsi-rns/tutor-11ty-bulma-md/raw/master/tutor-06/11ty-bulma-md-preview.png
