// Pagination links : Based on Glenn Mc Comb
// https://glennmccomb.com/articles/how-to-build-custom-hugo-pagination/
// Adjacent: Number of links either side of the current page

exports.isShowAdjacent = function(cursor, current, totalPages, linkOffset) {
  // initialize variables
  let showCursorFlag = false;

  const maxLinks      = (linkOffset * 2) + 1;
  const lowerLimit    = 1 + linkOffset;
  const upperLimit    = totalPages - linkOffset;
  
  if (totalPages > maxLinks) {
    // Complex page numbers.
    
    if (current <= lowerLimit) {
      // Lower limit pages.
      // If the user is on a page which is in the lower limit.
      if (cursor <= maxLinks)
         showCursorFlag = true;
    } else if (current >= upperLimit) {
      // Upper limit pages.
      // If the user is on a page which is in the upper limit.
      if (cursor > (totalPages - maxLinks))
         showCursorFlag = true;
    } else {
      // Middle pages.
      if ( (cursor >= current - linkOffset)
      &&   (cursor <= current + linkOffset) )
         showCursorFlag = true;
    }
  } else {
    // Simple page numbers.
    showCursorFlag = true;
  }

  return showCursorFlag;
}
