### Tutor 06

> Refactoring with Nunjucks

* Layout Inheritance: columns-double

* Nunjucks: Simplified All Layout Using Template Inheritance

* Partials: Index: Blog List, Tags, By Year, By Month

> Configuration

* Filter: mapdate, groupBy

* Collection: posts

* Shortcodes: excerpt

> Features

* More Content: Lyrics and Quotes. Need this content for demo

* Custom Output: JSON

> Decoration on Views

* View: Archive Index: Simple, By Year, By Month Tree.

* View: Tags Index: List Tree

* View: Nice Landing Page

* Content: Excerpt (<!--more-->): Read More Separator

![Eleventy Bulma MD: Tutor 05][11ty-bulma-md-preview-05]

-- -- --

What do you think ?

[11ty-bulma-md-preview-05]:https://gitlab.com/epsi-rns/tutor-11ty-bulma-md/raw/master/tutor-05/11ty-bulma-md-preview.png
