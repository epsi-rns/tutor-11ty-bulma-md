# Eleventy Bulma MD Test Drive

An example of Eleventy site using Bulma Material Design for personal learning purpose.

> Eleventy + Nunjucks + Bulma (Material Design)

![Eleventy Bulma MD: Tutor][11ty-bulma-md-preview]

-- -- --

## Links

### Eleventy Step By Step

> This repository:

* [Eleventy Step by Step Repository][tutorial-11ty-md]

### Eleventy Tutorial

A thorough Eleventy tutorial can be found in this article series:

* [Eleventy - Bulma MD - Overview][series-eleventy]

![Eleventy Bulma MD: Article Series][11ty-bulma-md-articles]

[tutorial-11ty-md]:     https://gitlab.com/epsi-rns/tutor-11ty-bulma-md/
[series-eleventy]:      https://epsi-rns.gitlab.io/ssg/2020/01/01/11ty-bulma-md-overview/

-- -- --

## Chapter Step by Step

### Tutor 01

> Running Eleventy for The First Time

* Configuration: Minimal

![Eleventy Bulma MD: Tutor 01][11ty-bulma-md-preview-01]

### Tutor 02

> Generate Only Pure HTML

* Configuration: Basic

* Setup Directory for Minimal Eleventy

* Basic Layout: Base, Page, Post

* Partials: Site Wide: HTML Head, Header, Footer

* Basic Content: Page and Post

* Nunjucks: Basic Loop

![Eleventy Bulma MD: Tutor 02][11ty-bulma-md-preview-02]

-- -- --

### Tutor 03

> Common Layout (without stylesheet)

* Configuration: Alias, Filter, Collection

* Additional Layout: Home, Archives, Index

* Additional Layout: Tags List and Generate Each Tag Name

* Basic View: Index, about

* Metadata

* Nunjucks: Tag Loop

![Eleventy Bulma MD: Tutor 03][11ty-bulma-md-preview-03]

-- -- --

### Tutor 04

> Add Pure Bulma CSS Framework

* Layout Inheritance (simple): columns-double

* Configuration: Move main configuration to above.

* Standard Header (jquery or vue) and Footer

* Enhance All Layouts with Bulma CSS

* Apply Two Column Responsive Layout for Most Layout

* Nunjucks: More Blocks

![Eleventy Bulma MD: Tutor 04][11ty-bulma-md-preview-04]

-- -- --

### Tutor 05

> Add Custom SASS (Custom Design)

* Layout Inheritance (with variables): columns-double

* Nunjucks: Simplified All Layout Using Template Inheritance

* Configuration: Nothing is changed.

* SASS: Bulma : Alter Navbar

* SASS: Spacing Helper, Google Material Color

* Nice Header and Footer

* Apply Google Material Pallete for All Layout

* Class: main-wrapper, aside-wrapper, blog-header, blog-body

* Nice Tag Badge in Tags Page

* Nunjucks: Nice post index using bulma inside loop

![Eleventy Bulma MD: Tutor 05][11ty-bulma-md-preview-05]

-- -- --

### Tutor 06

> Refactoring with Nunjucks

* Layout Inheritance: columns-double

* Partials: Index: Blog List, Tags, By Year, By Month

> Configuration

* Filter: mapdate, groupBy

* Collection: posts

* Shortcodes: excerpt

> Features

* More Content: Lyrics and Quotes. Need this content for demo

* Custom Output: JSON

> Decoration on Views

* View: Archives Index: Simple, By Year, By Month Tree.

* View: Tags Index: List Tree

* View: Nice Landing Page

* Content: Excerpt (<!--more-->): Read More Separator

![Eleventy Bulma MD: Tutor 06][11ty-bulma-md-preview-06]

-- -- --

### Tutor 07

> Features

* Blog Pagination: Adjacent, Indicator, Responsive

* Design: Apply Box Design, for Tag, and Archive

* Widget: Friends, Archives Tree, Categories, Tags, Recent Post, Related Post

> Refactoring with Nunjucks

* Layout Inheritance: single-double

* Partials: Page (blog-header), Post (blog-header), Widget (template)

* Nunjucks: Color using Template Inheritance

> Configuration

* Filter: limit, values, keys, shuffle, hashIt, pagerIt

* Collection: postsPrevNext

![Eleventy Bulma MD: Tutor 07][11ty-bulma-md-preview-07]

-- -- --

### Tutor 08

> Optional Feature

* Design: Apply Multi Column Responsive List, for Tag, and Archive

* Post: Header, Footer, Navigation

* Search: lunr.js

> Configuration

* Filter: keyJoin

* Some Shortcodes

> Blog Content

* Post: Markdown Content

* Post: Table of Content

* Post: Responsive Images

* Post: Shortcodes

* Post: Mathjax

* Post: Syntax Highlight: Bulma CSS Reset and Add Prism CSS

* Stylesheet: Bulma Title: CSS Fix

> Finishing

* Layout: Service

* Meta: HTML, SEO, Opengraph, Twitter

* RSS: Filter to Show Only Post Kind

* Metadata: Complete Author Info

![Eleventy Bulma MD: Tutor 08][11ty-bulma-md-preview-08]

-- -- --

### Template Inheritance Summary

Layout changes in each steps.

![Eleventy Bulma MD: Template Inheritance Summary][11ty-template-inheritance]

-- -- --

## Additional Links

### HTML Step By Step

Additional guidance:

* [Bulma Material Design Step by Step Repository][tutorial-bulma-md] (frankenbulma)

* [Bulma Step by Step Repository][tutorial-bulma]

* [Materialize Step by Step Repository][tutorial-materialize]

* [Semantic UI Step by Step Repository][tutorial-semantic-ui] (frankensemantic)

[tutorial-bulma-md]:    https://gitlab.com/epsi-rns/tutor-html-bulma-md/
[tutorial-bulma]:       https://gitlab.com/epsi-rns/tutor-html-bulma/
[tutorial-materialize]: https://gitlab.com/epsi-rns/tutor-html-materialize/
[tutorial-semantic-ui]: https://gitlab.com/epsi-rns/tutor-html-semantic-ui/

### Comparison

Comparation with other static site generator

* [Eleventy (Materialize) Step by Step Repository][tutorial-11ty-m]

* [Jekyll Step by Step Repository][tutorial-jekyll]

* [Hugo Step by Step Repository][tutorial-hugo]

* [Hexo Step by Step Repository][tutorial-hexo]

* [Pelican Step by Step Repository][tutorial-pelican]

[tutorial-pelican]:     https://gitlab.com/epsi-rns/tutor-pelican-bulma-md/
[tutorial-jekyll]:      https://gitlab.com/epsi-rns/tutor-jekyll-bulma-md/
[tutorial-hugo]:        https://gitlab.com/epsi-rns/tutor-hugo-bulma-md/
[tutorial-11ty-m]:      https://gitlab.com/epsi-rns/tutor-11ty-materialize/
[tutorial-hexo]:        https://gitlab.com/epsi-rns/tutor-hexo-bulma/

### Presentation

* [Concept SSG - Presentation Slide][ssg-presentation]

* [Concept CSS - Presentation Slide][css-presentation]

* [Eleventy - Presentation Slide][eleventy-presentation]

[ssg-presentation]:     https://epsi-rns.gitlab.io/ssg/2019/02/17/concept-ssg/
[css-presentation]:     https://epsi-rns.gitlab.io/frontend/2019/02/15/concept-css
[eleventy-presentation]:https://epsi-rns.gitlab.io/ssg/2020/02/07/11ty-presentation/

-- -- --

What do you think ?

[11ty-bulma-md-preview]:   https://gitlab.com/epsi-rns/tutor-11ty-bulma-md/raw/master/11ty-bulma-md-preview.png
[11ty-bulma-md-articles]:  https://gitlab.com/epsi-rns/tutor-11ty-bulma-md/raw/master/11ty-table-of-content.png
[11ty-bulma-md-preview-01]:https://gitlab.com/epsi-rns/tutor-11ty-bulma-md/raw/master/tutor-01/11ty-bulma-md-preview.png
[11ty-bulma-md-preview-02]:https://gitlab.com/epsi-rns/tutor-11ty-bulma-md/raw/master/tutor-02/11ty-bulma-md-preview.png
[11ty-bulma-md-preview-03]:https://gitlab.com/epsi-rns/tutor-11ty-bulma-md/raw/master/tutor-03/11ty-bulma-md-preview.png
[11ty-bulma-md-preview-04]:https://gitlab.com/epsi-rns/tutor-11ty-bulma-md/raw/master/tutor-04/11ty-bulma-md-preview.png
[11ty-bulma-md-preview-05]:https://gitlab.com/epsi-rns/tutor-11ty-bulma-md/raw/master/tutor-05/11ty-bulma-md-preview.png
[11ty-bulma-md-preview-06]:https://gitlab.com/epsi-rns/tutor-11ty-bulma-md/raw/master/tutor-06/11ty-bulma-md-preview.png
[11ty-bulma-md-preview-07]:https://gitlab.com/epsi-rns/tutor-11ty-bulma-md/raw/master/tutor-07/11ty-bulma-md-preview.png
[11ty-bulma-md-preview-08]:https://gitlab.com/epsi-rns/tutor-11ty-bulma-md/raw/master/tutor-08/11ty-bulma-md-preview.png

[11ty-template-inheritance]:https://gitlab.com/epsi-rns/tutor-11ty-bulma-md/raw/master/svg-inheritance/template-inheritance-animate.gif
