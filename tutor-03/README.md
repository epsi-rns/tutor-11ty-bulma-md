### Tutor 03

> Common Layout (without stylesheet)

* Configuration: Alias, Filter, Collection

* Additional Layout: Home, Archives, Index

* Additional Layout: Tags List and Generate Each Tag Name

* Basic View: Index, about

* Metadata

* Nunjucks: Tag Loop

![Eleventy Bulma MD: Tutor 02][11ty-bulma-md-preview-02]

-- -- --

What do you think ?

[11ty-bulma-md-preview-02]:https://gitlab.com/epsi-rns/tutor-11ty-bulma-md/raw/master/tutor-02/11ty-bulma-md-preview.png
