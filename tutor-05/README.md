### Tutor 05

> Add Custom SASS (Custom Design)

* Layout Inheritance (with variables): columns-double

* Configuration: Nothing is changed.

* SASS: Bulma : Alter Navbar

* SASS: Spacing Helper, Google Material Color

* Nice Header and Footer

* Apply Google Material Pallete for All Layout

* Class: main-wrapper, aside-wrapper, blog-header, blog-body

* Nice Tag Badge in Tags Page

* Nunjucks: Nice post index using bulma inside loop

![Eleventy Bulma MD: Tutor 04][11ty-bulma-md-preview-04]

-- -- --

What do you think ?

[11ty-bulma-md-preview-04]:https://gitlab.com/epsi-rns/tutor-11ty-bulma-md/raw/master/tutor-04/11ty-bulma-md-preview.png
