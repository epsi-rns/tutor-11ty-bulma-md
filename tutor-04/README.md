### Tutor 04

> Add Pure Bulma CSS Framework

* Layout Inheritance (simple): columns-double

* Configuration: Move main configuration to above.

* Standard Header (jquery or vue) and Footer

* Enhance All Layouts with Bulma CSS

* Apply Two Column Responsive Layout for Most Layout

* Nunjucks: More Blocks

![Eleventy Bulma MD: Tutor 03][11ty-bulma-md-preview-03]

-- -- --

What do you think ?

[11ty-bulma-md-preview-03]:https://gitlab.com/epsi-rns/tutor-11ty-bulma-md/raw/master/tutor-03/11ty-bulma-md-preview.png
